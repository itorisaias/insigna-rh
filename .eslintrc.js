module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ['plugin:vue/essential', '@vue/airbnb'],
  parserOptions: {
    parser: 'babel-eslint',
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    quotes: [2, 'single'],
    semi: [2, 'never'],
    'comma-dangle': [2, 'only-multiline'],
    'no-trailing-spaces': [2],
    'import/extensions': 'off',
    'import/no-named-as-default': 'off',
    'import/prefer-default-export': 'off',
    'arrow-parens': 'off',
    'import/no-cycle': 'off',
    'object-curly-newline': 'off',
    'linebreak-style': 'off',
  },
}
