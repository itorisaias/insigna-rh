import i18n from '@/plugins/i18n'

export default {
  required: v => !!v || i18n.t('rules.is_required'),
  maxLength: max => v => v.length <= max || i18n.t('rules.max_length'),
  minLength: min => v => v.length >= min || i18n.t('rules.min_length'),
  email: v => /.+@.+\..+/.test(v) || i18n.t('rules.is_email'),
}
