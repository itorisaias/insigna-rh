import faker from 'faker/locale/pt_BR'

export default [...new Array(20)].map((_, index) => ({
  id: index,
  name: faker.name.firstName(),
  results: {
    conscientiousness: faker.random.number({ min: 0, max: 100 }),
    openness: faker.random.number({ min: 0, max: 100 }),
    extraversion: faker.random.number({ min: 0, max: 100 }),
    drive: faker.random.number({ min: 0, max: 100 }),
    stability: faker.random.number({ min: 0, max: 100 }),
    agreeableness: faker.random.number({ min: 0, max: 100 }),
    outgoing: faker.random.number({ min: 0, max: 100 }),
    curious: faker.random.number({ min: 0, max: 100 }),
    disciplined: faker.random.number({ min: 0, max: 100 }),
    passionate: faker.random.number({ min: 0, max: 100 }),
    reserved: faker.random.number({ min: 0, max: 100 }),
    candid: faker.random.number({ min: 0, max: 100 }),
    flexible: faker.random.number({ min: 0, max: 100 }),
    pragmatic: faker.random.number({ min: 0, max: 100 }),
    considerate: faker.random.number({ min: 0, max: 100 }),
    stable: faker.random.number({ min: 0, max: 100 }),
    driven: faker.random.number({ min: 0, max: 100 }),
    'laid-back': faker.random.number({ min: 0, max: 100 }),
  },
}))
