import { mutation } from './types'

export default {
  [mutation.SET_LOADING]: (state, payload) => {
    state.loading = payload
  },
  [mutation.SET_ACCOUNT]: (state, payload) => {
    state.account = payload
  },
  [mutation.SET_ACCOUNTS]: (state, payload) => {
    state.accounts = payload
  },
  [mutation.SET_ERROR]: (state, payload) => {
    state.error = payload
  },
}
