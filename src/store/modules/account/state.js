export default {
  account: {
    name: null,
    cnpj: null,
    language: null,
    parent_account: null,
    active: false,
    id: null
  },
  accounts: [],
  error: null,
  loading: false,
}
