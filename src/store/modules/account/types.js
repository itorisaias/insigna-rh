export const mutation = {
  SET_ACCOUNT: 'setAccount',
  SET_ACCOUNTS: 'setAccounts',
  SET_LOADING: 'setLoading',
  SET_ERROR: 'setError',
}

export const actions = {
  GET_ACCOUNT: 'getAccount',
  GET_ACCOUNTS: 'getAccounts',
  CREATE_ACCOUNT: 'createAccount',
  UPDATE_ACCOUNT: 'updateAccount',
  DELETE_ACCOUNT: 'deleteAccount',
  ADD_ASSESSMENT: 'addAssessment',
}

export const getters = {
  ERROR: 'error',
  LOADING: 'loading',
  ACCOUNT: 'account',
  ACCOUNTS: 'accounts',
}
