import { getters } from './types'

export default {
  [getters.ACCOUNT]: state => state.account,
  [getters.ACCOUNTS]: state => state.accounts,
  [getters.ERROR]: state => state.error,
  [getters.LOADING]: state => state.loading,
}
