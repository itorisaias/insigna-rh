import * as accountService from '@/services/account'
import { mutation, actions } from './types'

export default {
  [actions.GET_ACCOUNT]: async ({ commit }, id) => {
    try {
      commit(mutation.SET_LOADING, true)

      const user = await accountService.findOne(id)

      commit(mutation.SET_ACCOUNT, user)
    } catch (error) {
      commit(mutation.SET_ERROR, error.message)
    } finally {
      commit(mutation.SET_LOADING, false)
    }
  },
  [actions.GET_ACCOUNTS]: async ({ commit }) => {
    try {
      commit(mutation.SET_LOADING, true)

      const users = await accountService.findAll({})

      commit(mutation.SET_ACCOUNTS, users)
    } catch (error) {
      commit(mutation.SET_ERROR, error.message)
    } finally {
      commit(mutation.SET_LOADING, false)
    }
  },
  [actions.CREATE_ACCOUNT]: async ({ commit, state }, payload) => {
    try {
      commit(mutation.SET_LOADING, true)

      const account = await accountService.create(payload)

      commit(mutation.SET_ACCOUNT, account)
      commit(mutation.SET_ACCOUNTS, [...state.accounts, account])
    } catch (error) {
      commit(mutation.SET_ERROR, error.message)
    } finally {
      commit(mutation.SET_LOADING, false)
    }
  },
  [actions.ADD_ASSESSMENT]: async ({ commit, state }, payload) => {
    try {
      commit(mutation.SET_LOADING, true)
      const { id } = state.account

      await accountService.addAssessment(id, payload)
    } catch (error) {
      commit(mutation.SET_ERROR, error.message)
    } finally {
      commit(mutation.SET_LOADING, false)
    }
  }
}
