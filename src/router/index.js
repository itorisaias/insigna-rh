import Vue from 'vue'
import VueRouter from 'vue-router'

import { authService } from '@/services'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Início',
    component: () => import('@/views/Home'),
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login'),
    meta: {
      guest: true,
    },
  },
  {
    path: '/404',
    component: () => import('@/views/NotFound'),
    meta: {
      guest: true,
    },
  },
  {
    path: '/user',
    name: 'Usuários',
    component: () => import('@/views/User/List'),
  },
  {
    path: '/user/detail/:id?',
    name: 'Detalhe de usuário',
    component: () => import('@/views/User/Detail'),
  },
  {
    path: '/account',
    component: () => import('@/views/Account/List'),
  },
  {
    path: '/profile',
    component: () => import('@/views/Profile'),
  },
  {
    path: '/account/detail/:id',
    component: () => import('@/views/Account/Detail'),
    children: [
      {
        path: 'data',
        component: () => import('@/views/Account/Detail/components/Data'),
      },
      {
        path: 'assessment',
        component: () => import('@/views/Account/Detail/components/Assessment'),
      },
      {
        path: 'settings',
        component: () => import('@/views/Account/Detail/components/Settings'),
      },
    ],
  },
  {
    path: '/assessment',
    name: 'Avaliações',
    component: () => import('@/views/Assessment/List'),
  },
  {
    path: '/assessment/detail/:id?',
    name: 'Detalhes da avaliação',
    component: () => import('@/views/Assessment/Detail'),
  },
  {
    path: '*',
    redirect: '/404',
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

router.beforeEach((to, from, next) => {
  if (authService.authenticated()) {
    return next()
  }

  return next('/login')
})

export default router
