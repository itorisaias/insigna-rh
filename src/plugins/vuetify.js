import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import pt from 'vuetify/es5/locale/pt'
import minifyTheme from 'minify-css-string'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: '#007491',
        text: '#404040',
        secondary: '#055473',
        accent: '#82B1FF',
        error: '#FF5252',
        success: '#4CAF50',
        warning: '#FFC107',
        background: '#F9F9F9',
      },
      dark: {
        primary: '#007491',
        secondary: '#055473',
        accent: '#82B1FF',
        error: '#FF5252',
        success: '#4CAF50',
        warning: '#FFC107',
      },
      options: { minifyTheme },
    },
    themeCache: {
      get: key => localStorage.getItem(key),
      set: (key, value) => localStorage.setItem(key, value),
    },
  },
  lang: {
    locales: { pt },
    current: 'pt',
  },
})
