import Vue from 'vue'

import Toast from '@/components/Toast'

const Alert = {
  install() {
    Vue.component('v-toast', Toast)

    Vue.prototype.$toast = Toast
  },
}

Vue.use(Alert)

export default Alert
