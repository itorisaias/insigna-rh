import i18n from './i18n'
import mask from './mask'
import vuetify from './vuetify'
import toast from './toast'

import './flagicon'
import './apexcharts'
import './logger'

export default {
  i18n,
  mask,
  vuetify,
  toast,
}
