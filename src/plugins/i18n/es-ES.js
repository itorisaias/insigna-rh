const messages = {
  menu: {
    home: 'Início',
    user: 'Usuários',
    account: 'Contas',
    assessment: 'Evaluación',
    profile: 'Perfil',
    request: 'Peticiones',
    logout: 'Cerrar sesión',
    mode_label: 'Modo oscuro',
  },
  pagination: {
    // FIXME
    all_select_page_label: 'All {count} {entity} on this page are selected.',
    all_select_page_action: 'Select all {count} {entity}.',
    all_select_records_label: 'All {count} {entity} are selected.',
    all_select_records_action: 'Clear selection.',
  },
  page: {
    user: {
      filter: {
        name: 'Nome',
        login: 'Login',
        email: 'E-mail',
        company: 'Empresa',
        active: 'Mostrar inativos'
      },
      form: {
        title: 'Novo usuário',
        fullname: 'Nome Completo',
        email: 'E-mail',
        company: 'Empresa',
        user_id_company: 'ID do usuário da empresa',
        login: 'Login',
        password: 'Senha',
        tax_id: 'CPF',
        language: 'Idioma',
        cost_center: 'Centro de custo',
        phone: 'Telefone',
        psychological: 'Permitir provas psicológicas?',
        gender: 'Gênero',
        date_of_birth: 'Data de nascimento',
        active: 'Ativo',
        responsible_label: 'Cadastre, abaixo, o email do(s) gestor(es) e RH(s) que devem receber o resultado da avaliação',
        responsible_email: 'Email responsável {i}',
        rule_label: 'Perfil de acesso',
        rules: ['Usuário', 'Parceiro RH', 'RH Master']
      },
      table: {
        name: 'Nombre',
        login: 'Login',
        email: 'E-mail',
        company: 'Empresa',
        language: 'Idioma'
      },
      action: {
        search: 'Buscar usuário',
        add: 'Criar usuário',
        save: 'Salvar',
        close: 'Fechar',
        import: 'Importação de usuário',
        clear: 'Limpar',
      }
    },
    login: {
      form: {
        username: 'Login',
        username_placeholder: 'Nombre de usuario',
        password: 'Contraseña',
        password_placeholder: 'Contraseña'
      },
      action: {
        login: 'Iniciar sesión',
        forgot_password: 'Olvide mi contraseña'
      },
    },
    account: {
      filter: {

      },
      form: {
        name: 'Nome da conta',
        parent_account: 'Conta parente',
        assessment: 'Avaliações'
      },
      table: {

      },
      action: {

      },
    }
  }
}

const dateTimeFormats = {
  short: {
    year: 'numeric',
    month: 'short',
    day: 'numeric'
  },
  long: {
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    weekday: 'short',
    hour: 'numeric',
    minute: 'numeric'
  }
}

const numberFormats = {
  currency: {
    style: 'currency', currency: 'EUR'
  }
}

export default {
  messages,
  dateTimeFormats,
  numberFormats
}
