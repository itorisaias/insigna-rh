const messages = {
  menu: {
    home: 'Início',
    user: 'Usuários',
    assessment: 'Avaliações',
    profile: 'Perfil',
    request: 'Solicitações',
    logout: 'Sair',
    account: 'Contas',
    mode_label: 'Modo escuro',
  },
  pagination: {
    all_select_page_label:
      'Todos os {count} {entity} nesta página estão selecionados.',
    all_select_page_action: 'Selecionar todos {count} {entity}.',
    all_select_records_label: 'Todos os {count} {entity} estão selecionados.',
    all_select_records_action: 'Limpar seleção.',
  },
  page: {
    user: {
      filter: {
        name: 'Nome',
        login: 'Login',
        email: 'E-mail',
        company: 'Empresa',
        active: 'Mostrar inativos',
      },
      form: {
        title: 'Novo usuário',
        fullname: 'Nome Completo',
        email: 'E-mail',
        company: 'Empresa',
        user_id_company: 'ID do usuário',
        login: 'Login',
        password: 'Senha',
        tax_id: 'CPF',
        language: 'Idioma',
        cost_center: 'Centro de custo',
        phone: 'Telefone',
        psychological: 'Permitir provas psicológicas?',
        gender: 'Gênero',
        date_of_birth: 'Data de nascimento',
        active: 'Ativo',
        responsible_label:
          'Cadastre, abaixo, o email do(s) gestor(es) e RH(s) que devem receber o resultado da avaliação',
        responsible_email: 'Email responsável {i}',
        rule_label: 'Perfil de acesso',
        rules: ['Usuário', 'Parceiro RH', 'RH Master'],
      },
      table: {
        name: 'Nome',
        login: 'Login',
        email: 'E-mail',
        company: 'Empresa',
        language: 'Idioma',
      },
      action: {
        search: 'Buscar usuário',
        add: 'Criar usuário',
        save: 'Salvar',
        close: 'Fechar',
        clear: 'Limpar',
        import: 'Importação de usuário',
      },
    },
    login: {
      form: {
        username: 'Login',
        username_placeholder: 'Nome do usuário',
        password: 'Senha',
        password_placeholder: 'Sua senha',
      },
      action: {
        login: 'Entrar',
        forgot_password: 'Esqueci minha senha',
      },
    },
    account: {
      filter: {

      },
      form: {
        name: 'Nome da conta',
        parent_account: 'Conta parente',
        assessment: 'Avaliações'
      },
      table: {

      },
      action: {

      },
    }
  },
}

const dateTimeFormats = {
  short: {
    year: 'numeric',
    month: 'short',
    day: 'numeric',
  },
  long: {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    weekday: 'long',
    hour: 'numeric',
    minute: 'numeric',
  },
}

const numberFormats = {
  currency: {
    style: 'currency',
    currency: 'BRL',
  },
}

export default {
  messages,
  dateTimeFormats,
  numberFormats,
}
