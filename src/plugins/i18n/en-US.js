const messages = {
  menu: {
    home: 'Home',
    user: 'Users',
    account: 'Account',
    assessment: 'Assessments',
    profile: 'Profile',
    request: 'Requests',
    logout: 'Logout',
    mode_label: 'Dark mode',
  },
  pagination: {
    all_select_page_label: 'All {count} {entity} on this page are selected.',
    all_select_page_action: 'Select all {count} {entity}.',
    all_select_records_label: 'All {count} {entity} are selected.',
    all_select_records_action: 'Clear selection.',
  },
  page: {
    user: {
      filter: {
        name: 'Name',
        login: 'Login',
        email: 'E-mail',
        company: 'Comapny',
        active: 'Show inactive'
      },
      form: {
        title: 'New user',
        fullname: 'Full name',
        email: 'E-mail',
        company: 'Comapny',
        user_id_company: 'Company user ID',
        login: 'Login',
        password: 'Password',
        tax_id: 'CPF',
        language: 'Language',
        cost_center: 'Cost center',
        phone: 'Phone',
        psychological: 'Allow psychological evidence?',
        gender: 'Gender',
        date_of_birth: 'Date of birth',
        active: 'Active',
        responsible_label: 'Register, below, the email of the manager and HR who should receive the result of the evaluation',
        responsible_email: 'Responsible email {i}',
        rule_label: 'Access Profile',
        rules: ['User', 'HR Partner', 'RH Master']
      },
      table: {
        name: 'Name',
        login: 'Login',
        email: 'E-mail',
        company: 'Company',
        language: 'Language'
      },
      action: {
        search: 'Search user',
        add: 'Create user',
        save: 'Save',
        close: 'Close',
        import: 'Import user',
        clear: 'Clear',
      }
    },
    login: {
      form: {
        username: 'Login',
        username_placeholder: 'Username',
        password: 'Password',
        password_placeholder: 'Your password'
      },
      action: {
        login: 'Login',
        forgot_password: 'Forgot password'
      },
    },
    account: {
      filter: {

      },
      form: {
        name: 'Nome da conta',
        parent_account: 'Conta parente',
        assessment: 'Avaliações'
      },
      table: {

      },
      action: {

      },
    }
  }
}

const dateTimeFormats = {
  short: {
    year: 'numeric',
    month: 'short',
    day: 'numeric'
  },
  long: {
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    weekday: 'short',
    hour: 'numeric',
    minute: 'numeric'
  }
}

const numberFormats = {
  currency: {
    style: 'currency', currency: 'USD'
  }
}

export default {
  messages,
  dateTimeFormats,
  numberFormats
}
