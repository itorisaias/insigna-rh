import Vue from 'vue'
import VueI18n from 'vue-i18n'

import ptBR from './pt-BR'
import enUS from './en-US'
import esES from './es-ES'

Vue.use(VueI18n)

const i18n = new VueI18n({
  locale: 'pt',
  fallbackLocale: 'en',
  messages: {
    pt: ptBR.messages,
    en: enUS.messages,
    es: esES.messages,
  }
})

export default i18n
