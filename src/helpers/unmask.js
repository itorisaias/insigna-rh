export default function unmask(value) {
  const unmaskedValue = value.replace(/[ ,.()-/]/g, '')

  return unmaskedValue
}
