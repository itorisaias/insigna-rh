export const validation = {
  required: v => !!v || 'Campo obrigatório',
  maxLength: max => v => v.length <= max || 'Campo inválido',
  minLength: min => v => v.length >= min || 'Campo inválido',
  email: v => /.+@.+\..+/.test(v) || 'E-mail inválido',
  cnpj: v => /.+@.+\..+/.test(v) || 'E-mail inválido',
}

export default validation
