import log from '@/utils/logger'
import { create as createApi } from '@/services/api'

const api = createApi('/assessment')

export const findAll = async filters => {
  try {
    const { data } = await api.get('/', {
      params: filters,
    })

    return data
  } catch (error) {
    log.error(error.response)
    throw new Error('unavailsable service find all assessment')
  }
}

export const findOne = async id => {
  try {
    const { data } = await api.get(`/${id}`)

    return data
  } catch (error) {
    log.error(error.response)
    throw new Error('unavailable service find one assessment')
  }
}

export const create = async payload => {
  try {
    const { data } = await api.post('/', payload)

    return data
  } catch (error) {
    log.error(error.response)
    throw new Error('unavailable service create assessment')
  }
}

export const update = async (id, payload) => {
  try {
    const { data } = await api.update(`/${id}`, payload)

    return data
  } catch (error) {
    log.error(error.response)
    throw new Error('unavailable service update assessment')
  }
}

// export const inviteAssessmentUsers = async () => {}

// export const compareUsersCandidates = async (users) => {}

// export const compareUsersTeams = async (teamA, teamB) => {}
