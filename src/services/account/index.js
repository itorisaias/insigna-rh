import log from '@/utils/logger'
import { create as createApi } from '@/services/api'

const api = createApi('/accounts')

/**
 * @param {{}} filters
 * @returns {Array<{
 * id: String,
 * name: String,
 * cnpj: String,
 * create_at: String,
 * update_at: String,
 * active: Boolean,
 * is_root: Boolean,
 * parent_account?: String,
 */
export const findAll = async filters => {
  try {
    const { data } = await api.get('/', {
      params: filters,
    })

    return data
  } catch (error) {
    log.error(error.response)
    throw new Error('unavailsable service find all account')
  }
}

/**
 *
 * @param {String} id
 * @returns {Array<{
 * id: String,
 * name: String,
 * cnpj: String,
 * create_at: String,
 * update_at: String,
 * active: Boolean,
 * is_root: Boolean,
 * parent_account?: {},
 */
export const findOne = async id => {
  try {
    const { data } = await api.get(`/${id}`)

    // const { data: parentAccount } = await api.get(`/${data.parent_account}`)

    return {
      ...data,
      // parent_account: parentAccount,
    }
  } catch (error) {
    log.error(error.response)
    throw new Error('unavailable service find one account')
  }
}

/**
 *
 * @param {{
 *  name: String,
 *  cnpj: String,
 *  assessments: {
 *   coreDrivers: { avaliable: Boolean },
 *   coreValues: { avaliable: Boolean },
 *   coreDriver: { avaliable: Boolean },
 *   cognify: { avaliable: Boolean },
 *   raciocionioLogico: { avaliable: Boolean },
 *  }
 * }} payload
 */
export const create = async payload => {
  try {
    const { data } = await api.post('/', payload)

    return data
  } catch (error) {
    log.error(error.response)
    throw new Error('unavailable service create account')
  }
}

/**
 *
 * @param {String} id
 * @param {{
 *  name: String,
 *  cnpj: String,
 *  assessments: {
 *   coreDrivers: { avaliable: Boolean },
 *   coreValues: { avaliable: Boolean },
 *   coreDriver: { avaliable: Boolean },
 *   cognify: { avaliable: Boolean },
 *   raciocionioLogico: { avaliable: Boolean },
 *   ptBrToPtBr: { avaliable: Boolean, skills: String[] },
 *   language: { avaliable: Boolean, skills: String[], lnaguagues: String[] },
 *  }
 * }} payload
 */
export const update = async (id, payload) => {
  try {
    const { data } = await api.patch(`/${id}`, payload)

    return data
  } catch (error) {
    log.error(error.response)
    throw new Error('unavailable service update account')
  }
}

/**
 *
 * @param {String} id
 * @param {{
 * name: String,
 * assessmentId: Number,
 * skillId: Number,
 * analyze: Boolean,
 * feedback: Boolean,
 * pdi: Boolean,
 * plano_meta: Boolean,
 * meta_estabelecida: Number,
 * prazo: Number,
 * meta_final: Number,
 * }} payload
 */
export const addAssessment = async (id, payload) => {
  try {
    // const { data } = await api.post(`/${id}/assessment`, payload)

    // return data
    console.log({ id, payload })
    return {}
  } catch (error) {
    log.error(error.response)
    throw new Error('unavailable service add assessment')
  }
}
