import axios from 'axios'

const BACKEND_URL = process.env.VUE_APP_SERVICE_URL

export const create = (path = '/') => {
  const api = axios.create({
    baseURL: `${BACKEND_URL}${path}`,
  })

  // api.interceptors.request.use(
  //   async config => {
  //     // const token = getToken()
  //     console.info('TODO', { config })
  //     // if (token) config.headers.Authorization = `Bearer ${token}`

  //     return config
  //   },
  //   error => {
  //     Promise.reject(error)
  //   },
  // )

  // api.interceptors.response.use(
  //   response => response,
  //   error => {
  //     // const originalRequest = error.config

  //     // if (originalRequest.url === '/auth_google/refresh_token') {
  //     //   router.push('/login')

  //     //   clearTokens()

  //     //   return Promise.reject(error)
  //     // }

  //     // if (error.response.status === 401) { // && !originalRequest._retry
  //       // originalRequest._retry = true

  //       // await refresh()

  //       // return api(originalRequest)
  //     // }
  //     console.error('TODO', error.response)

  //     return Promise.reject(error)
  //   },
  // )

  return api
}
