import * as authService from './auth'
import * as userService from './user'
import * as assessmentService from './assessment'
import * as accountService from './account'

export {
  authService,
  userService,
  assessmentService,
  accountService
}
