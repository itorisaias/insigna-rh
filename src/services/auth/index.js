// import log from '@/utils/logger'

const TOKEN_NAME = 'insigna_token'

export const setToken = (token) => localStorage.setItem(TOKEN_NAME, token)

export const getToken = () => localStorage.getItem(TOKEN_NAME)

export const clearToken = () => localStorage.removeItem(TOKEN_NAME)

export const authenticated = (token = getToken()) => !!token

/**
 *
 * @param {{
 * username: String,
 * password: String
 * }} credentials
 *
 * @returns {{
 * accessToken: String,
 * userName: String
 * }}
 */
export const login = async ({ username, password }) => {
  console.info('TODO Login', { username, password })

  const accessToken = Math.floor(Math.random() * 1000)
  const userName = 'Itor Isaias'

  const req = new Promise((resolve) => {
    setTimeout(() => {
      resolve({ accessToken, userName })
    }, 5000)
  })

  const data = await req

  setToken(data.accessToken)

  return data
}

export const logout = async () => {
  try {
    console.info('TODO Logout')

    const req = new Promise((resolve) => {
      setTimeout(() => {
        resolve('OK')
      }, 3000)
    })

    await req
  } catch (error) {
    console.error(error)
  }

  clearToken()
}
