import log from '@/utils/logger'
import { create as createApi } from '@/services/api'

const api = createApi('/users')

export const findAll = async filters => {
  try {
    const { data } = await api.get('/', {
      params: filters,
    })

    return data
  } catch (error) {
    log.error(error)
    throw new Error('Unavailable service find all users')
  }
}

export const findOne = async id => {
  try {
    const { data } = await api.get(`/${id}`)

    return data
  } catch (error) {
    log.error(error)
    throw new Error('unavailable service find one user')
  }
}

export const create = async payload => {
  try {
    const { data } = await api.post('/', payload)

    return data
  } catch (error) {
    log.error(error)
    throw new Error('unavailable service create user')
  }
}

export const update = async (id, payload) => {
  try {
    const { data } = await api.update(`/${id}`, payload)

    return data
  } catch (error) {
    log.error(error)
    throw new Error('unavailable service update user')
  }
}

// export const createUsersGroup = async (users, groupId) => {}

// export const deleteUsersGroup = async (users, groupId) => {}

// export const inviteUsersAssessments = async (users, assessments) => {}
