const faker = require('faker/locale/pt_BR')
const cpf = require('@fnando/cpf/commonjs')
const fs = require('fs')
const { fake } = require('faker/locale/pt_BR')

const users = []

function randomInt(max) {
  return Math.floor(Math.random() * max)
}
function random(list) {
  return list[randomInt(list.length)]
}

const empresas = ['Bradesco São Paulo', 'Bradesco Rio de Janeiro', 'Bradesco Campinas']
const languages = ['pr-BR', 'en-US', 'es-ES']
const genders = ['female', 'male']
const roles = ['user', 'partner', 'admin']
const incomes = [
  'under de 23.000',
  '23.000 - 60.000',
  '60.000 - 90.000',
  'over 90.000'
]
const educationLevels = ['high_school', 'associates', 'under_graduate', 'masters', 'doctorate']
const jobLevels = ['student', 'individual_contributor', 'middle_management', 'executive']

for (let i = 0; i < 50; i++) {
  const user = {
    id: i,
    first_name: faker.name.firstName(),
    last_name: faker.name.lastName(),
    email: faker.internet.email(),
    account: {
      id: faker.random.number(),
      name: random(empresas)
    },
    language: random(languages),
    active: faker.random.boolean(),
    create_at: faker.date.recent(randomInt(30)),
    update_at: faker.date.recent(randomInt(30)),
    last_login_at: faker.date.recent(randomInt(30)),

    gender: random(genders),

    birth_year: faker.date.past(18).getFullYear(),
    country: 'Brasil',
    state: faker.address.state(),

    education_level: random(educationLevels),
    annual_income: random(incomes),
    job_level: random(jobLevels),

    role: random(roles),

    groups: [...new Array(faker.random.number(3))].map((_, j) => ({
      id: j,
      name: random(['Grupo A', 'Grupo B', 'Grupo C', 'Grupo D'])
    }))
  }

  users.push(user)
}

const file = `${__dirname}/mock.json`

const content = JSON.stringify({ users }, null, 2)

fs.writeFileSync(file, content, { encoding: 'utf-8' })
