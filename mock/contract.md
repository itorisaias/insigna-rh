# Contratos

## Account
Get all users
```
request
  path: /api/v1/accounts
  query: { email?: string, account?: string, active: true }

response
[
  {
    id: 1,
    firstName: 'Itor',
    lastName: 'Isaias',
    email: 'itor.isaias@gmail.com',
    account: {
      id: 30,
      name: 'Empresa X'
    },
    language: 'pt-BR',
    active: true,
    create_at: '2020-07-06T19:49:57.140Z',
    update_at: '2020-07-06T19:49:57.140Z',
    last_login_at: '2020-07-06T19:49:57.140Z',
    gender: 'male',
    birth_year: '1996',
    country: 'Brasil',
    state: 'São Paulo',
    education_level: '',
    annual_income: '',
    job_level: '',
    role: 'user',
    groups: [
      { id: 3, name: 'Grupo A' },
      { id: 4, name: 'Grupo B' }
    ]
  }
]
```

Get one
```
request
  path: /api/v1/accounts/1
  query: { email?: string, account?: string, active: true }

response
{
  id: 1,
  firstName: 'Itor',
  lastName: 'Isaias',
  email: 'itor.isaias@gmail.com',
  account: {
    id: 30,
    name: 'Empresa X'
  },
  language: 'pt-BR',
  active: true,
  create_at: '2020-07-06T19:49:57.140Z',
  update_at: '2020-07-06T19:49:57.140Z',
  last_login_at: '2020-07-06T19:49:57.140Z',
  gender: 'male',
  birth_year: '1996',
  country: 'Brasil',
  state: 'São Paulo',
  education_level: '',
  annual_income: '',
  job_level: '',
  role: 'user',
  groups: [
    { id: 3, name: 'Grupo A' },
    { id: 4, name: 'Grupo B' }
  ],
  assessments: [
    {
      id: 7,
      name: 'Avaliação semestral',
      assessment: {
        id: 99,
        name: 'The core drivers'
      },
      started_at: '2020-07-06T19:49:57.140Z',
      completed_at: '2020-07-06T19:49:57.140Z',
      assigned_at: '2020-07-06T19:49:57.140Z',
      status: 'not completed'
    },
    {
      id: 7,
      name: 'Avaliação semestral',
      assessment: {
        id: 99,
        name: 'english language',
        skills: [
          'Ler',
          'Escrever'
        ]
      },
      started_at: '2020-07-06T19:49:57.140Z',
      completed_at: '2020-07-06T19:49:57.140Z',
      assigned_at: '2020-07-06T19:49:57.140Z',
      status: 'completed'
    }
  ]
}
```

Create
```
  
```
## User

## Assessment
